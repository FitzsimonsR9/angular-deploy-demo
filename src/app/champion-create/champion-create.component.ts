import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-champion-create',
  templateUrl: './champion-create.component.html',
  styleUrls: ['./champion-create.component.css']
})
export class ChampionCreateComponent implements OnInit {

  @Input() championDetails = { id: 0, name: '', difficulty: '', role: '', imageUrl: '' }

  constructor(
    public restApi: RestApiService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  addChampion(){
    this.restApi.createChampion(this.championDetails).subscribe((data: {}) => {
      this.router.navigate(['/champion-list'])
    })
  }

}
