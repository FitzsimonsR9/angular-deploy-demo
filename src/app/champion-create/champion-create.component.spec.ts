import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChampionCreateComponent } from './champion-create.component';

describe('ChampionCreateComponent', () => {
  let component: ChampionCreateComponent;
  let fixture: ComponentFixture<ChampionCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChampionCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChampionCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
