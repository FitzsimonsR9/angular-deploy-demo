import { VirtualTimeScheduler } from "rxjs";

export class Champion {
  difficulty: string;
  id: number;
  imageUrl: string;
  name: string;
  role: string;

  constructor(){
    this.difficulty="";
    this.id=0;
    this.imageUrl="";
    this.name="";
    this.role="";
  }
}
