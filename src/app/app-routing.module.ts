import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChampionCreateComponent } from './champion-create/champion-create.component';
import { ChampionEditComponent } from './champion-edit/champion-edit.component';
import { ChampionListComponent } from './champion-list/champion-list.component';

const routes: Routes = [
  {path:'', pathMatch: 'full', redirectTo: 'champion-list'},
  {path:'create-champion', component: ChampionCreateComponent},
  {path:'champion-list', component: ChampionListComponent},
  {path:'champion-edit/:id', component: ChampionEditComponent},
  {path:'**', component: ChampionListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
