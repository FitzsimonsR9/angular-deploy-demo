import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-champion-edit',
  templateUrl: './champion-edit.component.html',
  styleUrls: ['./champion-edit.component.css']
})
export class ChampionEditComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  championDetails: any = {};

  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.restApi.getChampion(this.id).subscribe((data: {}) => {
      this.championDetails = data;
    })
  }

  updateChampion(){
    if(window.confirm('Are you sure, you want to update?')){
      this.restApi.updateChampion(this.id, this.championDetails).subscribe(data => {
        this.router.navigate(['/champion-list'])
      })
    }
  }

}
